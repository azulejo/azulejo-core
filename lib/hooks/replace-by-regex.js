const nil = require('ramda/src/isNil')

module.exports = (fieldName, patterns = [], defPattern = [/\s{2,}/g, ' ']) => (ctx) => {
  if (!fieldName) throw new TypeError(`${fieldName} is not specified`)

  const field = ctx.data[fieldName]
  if (nil(field)) throw new TypeError(`${fieldName} is nil`)

  ctx.data = {
    ...ctx.data,
    [fieldName]: patterns
      .concat([defPattern])
      .reduce(
        (acc, it) => acc.replace(it[0], it[1]),
        field
      ),
  }

  return ctx
}
