const nil = require('ramda/src/isNil')

const translit = require('../utils/transliterate')

// from → to
module.exports = (fr, to, map) => (ctx) => {
  const field = ctx.data[fr]
  if (nil(field)) throw new TypeError(`${fr} is nil`)

  ctx.data = {
    ...ctx.data,
    [to]: translit(field, map),
  }

  return ctx
}
