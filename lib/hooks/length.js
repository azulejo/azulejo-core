const nil = require('ramda/src/isNil')

module.exports = (field = 'uri', length = 256) => (ctx) => {
  const _field = ctx.data[field]
  if (nil(_field)) throw new TypeError(`${field} is nil`)

  if (!_field.toString || _field.length > length) throw new Error(`${field} has invalid length`)

  return ctx
}
