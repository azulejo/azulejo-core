const cond = require('ramda/src/cond')
const equals = require('ramda/src/equals')
const T = require('ramda/src/T')

module.exports = () => (ctx) => {
  const { params: { user } } = ctx
  const chunk = cond([
    [equals('create'), () => ({ createdby: user.id })],
    [T, () => ({ editedby: user.id })],
  ])

  ctx.data = {
    ...ctx.data,
    ...chunk(ctx.method),
  }

  return ctx
}
