
module.exports = (prefix = '_') => (ctx) => {
  ctx.data = Object.keys(ctx.data).reduce((acc, next) => {
    if (!next.startsWith(prefix)) acc[next] = ctx.data[next]

    return acc
  }, {})

  return ctx
}
