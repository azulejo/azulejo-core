module.exports = () => (ctx) => {
  ctx.data = Object.keys(ctx.data).reduce((acc, it) => {
    acc[it] = acc[it] && acc[it].trim ? acc[it].trim() : acc[it]

    return acc
  }, ctx.data)

  return ctx
}
