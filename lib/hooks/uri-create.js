const cond = require('ramda/src/cond')
const equals = require('ramda/src/equals')
const nil = require('ramda/src/isNil')

const _fields = {
  alias: 'alias',
  folder: 'isfolder',
  parent: 'parent',
}

const _service = (ctx, serviceName) => serviceName ? ctx.app.service(serviceName) : ctx.service

const createQuery = (ctx, fieldName, fields, serviceName) => {
  if (['update', 'patch'].includes(ctx.method)) {
    if (!ctx.id) throw new Error('id is not specified')

    return _service(ctx, serviceName)
      .createQuery()
      .select(fieldName, fields.parent)
      .where({ id: ctx.id })
  }

  if (ctx.method === 'create') {
    const parent = ctx.data[fields.parent]
    if (nil(parent)) throw new Error(`${fields.parent} is not specified`)

    return Promise.resolve([
      {
        [fields.parent]: parent,
        [fieldName]: '',
      },
    ])
  }

  throw new Error('Can not use hook on wrong method')
}

const requestParentAlias = async (aliases = [], parent, service) => {
  if (Number(parent) === 0) return aliases

  const [record] = await service.createQuery()
    .select(['alias', 'parent'])
    .where({ id: parent })

  return requestParentAlias([record.alias].concat(aliases), record.parent, service)
}

// mime -> extension
const contentType = cond([
  [equals('text/html'), () => '.html'],
  [equals('text/xml'), () => '.xml'],
  [equals('text/plain'), () => '.txt'],
])

module.exports = (fieldName = 'uri', fields = _fields, serviceName = 'resources') => (ctx) => {
  const alias = ctx.data[fields.alias]
  const folder = ctx.data[fields.folder]

  if (nil(alias)) throw new TypeError(`${fields.alias} is not specified`)
  if (nil(folder)) throw new TypeError(`${fields.folder} is not specified`)

  return createQuery(ctx, fieldName, fields, serviceName)
    // eslint-disable-next-line promise/no-nesting
    .then(that => requestParentAlias([alias], that[0][fields.parent], _service(ctx, serviceName))
      .then((aliases) => {
        const old = that[0].uri
        const uri = `${aliases.join('/')}${folder ? '/' : contentType(ctx.data.contentType)}`
        // TODO: do smth for folders with `/` on end

        if (old && old !== uri) ctx.data[`_${fieldName}`] = old
        // save old uri if it will be changed

        ctx.data[fieldName] = uri

        return ctx
      }))
}
