const cond = require('ramda/src/cond')
const equals = require('ramda/src/equals')
const T = require('ramda/src/T')

module.exports = () => (ctx) => {
  const timestamp = Math.round(Date.now() / 1e3)

  const chunk = cond([
    [equals('create'), () => ({ createdon: timestamp })],
    [T, () => ({ editedon: timestamp })],
  ])

  ctx.data = {
    ...ctx.data,
    ...chunk(ctx.method),
  }

  return ctx
}
