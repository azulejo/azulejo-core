module.exports = () => (ctx) => {
  const def = new Map([
    ['published', 1],
    ['deleted', 0],
    ['hidemenu', 0],
    ['isfolder', 0],
    ['searchable', 1],
    ['type', 'document'],
    ['contentType', 'text/html'],
    ['cacheable', '1'],
    ['template', '1'],
  ])

  def.forEach((v, k) => {
    ctx.data[k] = ctx.data[k] || v
  })

  return ctx
}
