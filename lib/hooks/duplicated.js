const createQuery = (ctx, field, serviceName) => {
  let q = (serviceName ? ctx.app.service(serviceName) : ctx.service)
    .createQuery()
    .where({
      [field]: ctx.data[field],
      deleted: 0,
    })

  if (ctx.method !== 'create') {
    if (!ctx.id) throw new Error('id is not specified')

    q = q.whereNot({
      id: ctx.id,
    })
  }

  return q
}

module.exports = function isFieldDuplicated (field = 'alias', serviceName) { // eslint-disable-line no-unused-vars
  return (ctx) => {
    if (typeof ctx.data[field] === 'undefined') throw new Error(`${field} is not specified`)

    return createQuery(ctx, field, serviceName)
      .then(([hasDuplicates]) => {
        if (hasDuplicates) throw new TypeError(`Can not create the record. ${field} is duplicated`)

        return ctx
      })
  }
}
