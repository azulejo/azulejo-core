const { normalize } = require('uri-js')
const nil = require('ramda/src/isNil')

const { encode } = require('../utils')

module.exports = fieldName => (ctx) => {
  if (!fieldName) throw new TypeError(`${fieldName} is not specified`)

  const field = ctx.data[fieldName]
  if (nil(field)) throw new TypeError(`${fieldName} is nil`)

  ctx.data[fieldName] = normalize(`//${encode(field)}`).slice(2)

  return ctx
}
