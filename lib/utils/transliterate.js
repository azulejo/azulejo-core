const map = {
  ' ': ' ',
  '%': null,
  '&': 'and',
  '/': '-',
  '\'': null,
  1100: null,
  À: 'A',
  Á: 'A',
  Â: 'A',
  Ã: 'A',
  Ä: 'e',
  Å: 'A',
  Æ: 'e',
  Ç: 'C',
  È: 'E',
  É: 'E',
  Ê: 'E',
  Ë: 'E',
  Ì: 'I',
  Í: 'I',
  Î: 'I',
  Ï: 'I',
  Ñ: 'N',
  Ò: 'O',
  Ó: 'O',
  Ô: 'O',
  Õ: 'O',
  Ö: 'e',
  Ø: 'O',
  Ù: 'U',
  Ú: 'U',
  Û: 'U',
  Ü: 'e',
  ß: 's',
  à: 'a',
  á: 'a',
  â: 'a',
  ã: 'a',
  ä: 'e',
  å: 'a',
  æ: 'e',
  ç: 'c',
  è: 'e',
  é: 'e',
  ê: 'e',
  ë: 'e',
  ì: 'i',
  í: 'i',
  î: 'i',
  ï: 'i',
  ñ: 'n',
  ò: 'o',
  ó: 'o',
  ô: 'o',
  õ: 'o',
  ö: 'e',
  ø: 'o',
  ù: 'u',
  ú: 'u',
  û: 'u',
  ü: 'e',
  ÿ: 'y',
  Ā: 'A',
  ā: 'a',
  Ă: 'A',
  ă: 'a',
  Ą: 'A',
  ą: 'a',
  Ć: 'C',
  ć: 'c',
  Ĉ: 'C',
  ĉ: 'c',
  Ċ: 'C',
  ċ: 'c',
  Č: 'C',
  č: 'c',
  Ď: 'D',
  ď: 'd',
  Đ: 'D',
  đ: 'd',
  Ē: 'E',
  ē: 'e',
  Ĕ: 'E',
  ĕ: 'e',
  Ė: 'E',
  ė: 'e',
  Ę: 'E',
  ę: 'e',
  Ě: 'E',
  ě: 'e',
  Ĝ: 'G',
  ĝ: 'g',
  Ğ: 'G',
  ğ: 'g',
  Ġ: 'G',
  ġ: 'g',
  Ģ: 'G',
  ģ: 'g',
  Ĥ: 'H',
  ĥ: 'h',
  Ħ: 'H',
  ħ: 'h',
  Ĩ: 'I',
  ĩ: 'i',
  Ī: 'I',
  ī: 'i',
  Ĭ: 'I',
  ĭ: 'i',
  Į: 'I',
  į: 'i',
  İ: 'I',
  ı: 'i',
  Ĳ: 'J',
  ĳ: 'j',
  Ĵ: 'J',
  ĵ: 'j',
  Ķ: 'K',
  ķ: 'k',
  ĸ: 'k',
  Ĺ: 'K',
  ĺ: 'l',
  Ļ: 'K',
  ļ: 'l',
  Ľ: 'K',
  ľ: 'l',
  Ŀ: 'K',
  ŀ: 'l',
  ł: 'l',
  Ń: 'N',
  ń: 'n',
  Ņ: 'N',
  ņ: 'n',
  Ň: 'N',
  ň: 'n',
  ŉ: 'n',
  Ŋ: 'N',
  ŋ: 'n',
  Ō: 'O',
  ō: 'o',
  Ŏ: 'O',
  ŏ: 'o',
  Ő: 'O',
  ő: 'o',
  Œ: 'E',
  œ: 'e',
  Ŕ: 'R',
  ŕ: 'r',
  Ŗ: 'R',
  ŗ: 'r',
  Ř: 'R',
  ř: 'r',
  Ś: 'S',
  Ŝ: 'S',
  Ş: 'S',
  Ţ: 'T',
  Ť: 'T',
  Ŧ: 'T',
  Ũ: 'U',
  ũ: 'u',
  Ū: 'U',
  ū: 'u',
  Ŭ: 'U',
  ŭ: 'u',
  Ů: 'U',
  ů: 'u',
  Ű: 'U',
  ű: 'u',
  Ų: 'U',
  ų: 'u',
  Ŵ: 'W',
  ŵ: 'w',
  Ŷ: 'Y',
  ŷ: 'y',
  Ÿ: 'Y',
  Ź: 'Z',
  ź: 'z',
  Ż: 'Z',
  ż: 'z',
  ſ: 's',
  ƒ: 'f',
  Ș: 'S',
  Ț: 'T',
  Ά: 'A',
  Έ: 'E',
  Ή: 'I',
  Ί: 'I',
  Ό: 'O',
  Ύ: 'Y',
  Ώ: 'O',
  ΐ: 'i',
  Α: 'A',
  Β: 'B',
  Γ: 'G',
  Δ: 'D',
  Ε: 'E',
  Ζ: 'Z',
  Η: 'I',
  Θ: 'TH',
  Ι: 'I',
  Κ: 'K',
  Λ: 'L',
  Μ: 'M',
  Ν: 'N',
  Ξ: 'KS',
  Ο: 'O',
  Π: 'P',
  Ρ: 'R',
  Σ: 'S',
  Τ: 'T',
  Υ: 'Y',
  Φ: 'F',
  Χ: 'X',
  Ψ: 'PS',
  Ω: 'O',
  Ϊ: 'I',
  Ϋ: 'Y',
  ά: 'a',
  έ: 'e',
  ή: 'i',
  ί: 'i',
  ΰ: 'y',
  α: 'a',
  β: 'b',
  γ: 'g',
  δ: 'd',
  ε: 'e',
  ζ: 'z',
  η: 'i',
  θ: 'th',
  ι: 'i',
  κ: 'k',
  λ: 'l',
  μ: 'm',
  ν: 'n',
  ξ: 'ks',
  ο: 'o',
  π: 'p',
  ρ: 'r',
  σ: 's',
  τ: 't',
  υ: 'y',
  φ: 'f',
  χ: 'x',
  ψ: 'ps',
  ω: 'o',
  ϊ: 'i',
  ϋ: 'y',
  ό: 'o',
  ύ: 'y',
  ώ: 'o',
  Ё: 'yo',
  А: 'a',
  Б: 'b',
  В: 'v',
  Г: 'g',
  Д: 'd',
  Е: 'e',
  Ж: 'zh',
  З: 'z',
  И: 'i',
  Й: 'j',
  К: 'k',
  Л: 'l',
  М: 'm',
  Н: 'n',
  О: 'o',
  П: 'p',
  Р: 'r',
  С: 's',
  Т: 't',
  У: 'u',
  Ф: 'f',
  Х: 'x',
  Ц: 'cz',
  Ч: 'ch',
  Ш: 'sh',
  Щ: 'shh',
  Ъ: null,
  Ы: 'yi',
  Ь: null,
  Э: 'e',
  Ю: 'yu',
  Я: 'ya',
  а: 'a',
  б: 'b',
  в: 'v',
  г: 'g',
  д: 'd',
  е: 'e',
  ж: 'zh',
  з: 'z',
  и: 'i',
  й: 'j',
  к: 'k',
  л: 'l',
  м: 'm',
  н: 'n',
  о: 'o',
  п: 'p',
  р: 'r',
  с: 's',
  т: 't',
  у: 'u',
  ф: 'f',
  х: 'x',
  ц: 'cz',
  ч: 'ch',
  ш: 'sh',
  щ: 'shh',
  ъ: null,
  ы: 'yi',
  ь: null,
  э: 'e',
  ю: 'yu',
  я: 'ya',
  ё: 'yo',
}

module.exports = (input = '', substitute = map) => {
  const byCode = x => substitute[x.charCodeAt()] || null

  return Array.prototype
    .slice
    .call(input.toString())
    .map((x) => {
      const repl = substitute[x]

      return byCode(x) || ((repl === null || repl) ? repl : x)
    })
    .join('')
}
