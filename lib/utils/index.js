const isNotComponent = (x, ignore) => x.includes('/') && !ignore

exports.encode = (x, ignore = false) => (isNotComponent(x, ignore) ? encodeURI : encodeURIComponent)(x)

exports.decode = (x, ignore = false) => (isNotComponent(x, ignore) ? decodeURI : decodeURIComponent)(x)
