module.exports = (sanitize) => {
  if (typeof sanitize !== 'function') throw new TypeError('Wrong sanitizer')

  return (map, keys = []) => keys.reduce(
    (acc, it) => !acc[it]
      ? acc
      : Object.assign(acc, { [it]: sanitize(acc[it]) }),
    map
  )
}
